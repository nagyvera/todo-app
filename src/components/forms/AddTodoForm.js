import React, { useState } from 'react'

const AddTodoForm = props => {
    const initialFormState = { id: null, name: '', date: '', details: '' }
    const [ todo, setTodo ] = useState(initialFormState)

    const handleInputChange = event => {
        const { name, value } = event.target

        setTodo({ ...todo, [name]: value })
    }

    return (
        <form
            onSubmit={event => {
                event.preventDefault()
                if (!todo.name || !todo.date) return

                props.addTodo(todo)
                setTodo(initialFormState)
            }}
        >
            <label>Tennivaló neve</label>
            <input type="text" name="name" value={todo.name} onChange={handleInputChange} />
            <label>Határidő</label>
            <input type="date" name="date" value={todo.date} onChange={handleInputChange} />
            <label>Részletek</label>
            <input type="details" className="detail" name="details" value={todo.details} onChange={handleInputChange} />
            <button>Hozzáadás</button>
        </form>
    )
}

export default AddTodoForm
