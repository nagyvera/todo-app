import React, { useState, useEffect } from 'react'

const EditTodoForm = props => {
  const [ todo, setTodo ] = useState(props.currentTodo)

  useEffect(
      () => {
        setTodo(props.currentTodo)
      },
      [ props ]
    )

  const handleInputChange = event => {
    const { name, value } = event.target
    setTodo({ ...todo, [name]: value })
  }

  return (
    <form
      onSubmit={event => {
        event.preventDefault()

        props.updateTodo(todo.id, todo)
      }}
    >
      <label>Teendő neve</label>
      <input type="text" name="name" value={todo.name} onChange={handleInputChange} />
      <label>Határidő</label>
      <input type="text" name="date" value={todo.date} onChange={handleInputChange} />
      <label>Részletek</label>
            <input type="text" name="details" value={todo.details} onChange={handleInputChange} />
      <button>Teendő frissítése</button>
      <button onClick={() => props.setEditing(false)} className="button muted-button">
        Mégse
      </button>
    </form>
  )
}

export default EditTodoForm
