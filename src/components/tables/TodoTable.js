import React from 'react';

const TodoTable = props => (
    <table>
        <thead>
        <tr>
            <th>Teendő neve</th>
            <th>Határidő</th>
            <th>Részletek</th>
            <th>Műveletek</th>
        </tr>
        </thead>
        <tbody>
        {props.todos.length > 0 ? (
            props.todos.map(todo => (
                <tr key={todo.id}>
                    <td>{todo.name}</td>
                    <td>{todo.date}</td>
                    <td>{todo.details}</td>
                    <td>
                        <button
                            onClick={() => {
                                props.editRow(todo)
                            }}
                            className="btn btn-warning"
                        >
                            Módosítás
                        </button>
                        <button
                            onClick={() => props.deleteTodo(todo.id)}
                            className="btn btn-danger"
                        >
                            Törlés
                        </button>
                    </td>
                </tr>
            ))
        ) : (
            <tr>
                <td colSpan={3}>Nincs teendő</td>
            </tr>
        )}
        </tbody>
    </table>
)

export default TodoTable
