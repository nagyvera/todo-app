import React, {Fragment, useState} from 'react';

import EditTodoForm from './forms/EditTodoForm';
import AddTodoForm from "./forms/AddTodoForm";
import TodoTable from "./tables/TodoTable";

const Home = () => {
    const todosData =[
        { id: 1, name: 'Bevásárlás', date: '2020-06-17', details: 'kenyér, tej, alma' },
    ];

    const initialFormState = { id: null, name: '', date: '', details:'' }

    // Setting state
    const [ todos, setTodos ] = useState(todosData)
    const [ currentTodo, setCurrentTodo ] = useState(initialFormState)
    const [ editing, setEditing ] = useState(false)

    // CRUD operations
    const addTodo = todo => {
        todo.id = todos.length + 1
        setTodos([ ...todos, todo ])
    }

    const deleteTodo = id => {
        setEditing(false)

        setTodos(todos.filter(todo => todo.id !== id))
    }

    const updateTodo = (id, updatedTodo) => {
        setEditing(false)

        setTodos(todos.map(todo => (todo.id === id ? updatedTodo : todo)))
    }

    const editRow = todo => {
        setEditing(true)

        setCurrentTodo({ id: todo.id, name: todo.name, date: todo.date, details: todo.details })
    }

    return(
        <div className= "welcome">
                <h1>To Do List</h1>
                <p> Jegyezd fel a tennivalóidat, hogy megkönnyítsd a napod! <i className="fas fa-heart"></i></p>
        <div className="container">
                <div className="flex-row">
                <div className="flex-large">
                    {editing ? (
                        <Fragment>
                            <h2>Módosítás</h2>
                            <EditTodoForm
                                editing={editing}
                                setEditing={setEditing}
                                currentTodo={currentTodo}
                                updateTodo={updateTodo}
                            />
                    </Fragment>
                ) : (
                    <Fragment>
                        <h3>Teendő hozzáadása</h3>
                        <AddTodoForm addTodo={addTodo} />
                    </Fragment>
                )}
            </div>
            <div className="flex-large">
                <h3>Teendők listája</h3>
                <TodoTable todos={todos} editRow={editRow} deleteTodo={deleteTodo} />
            </div>
        </div>
    </div>
    </div>
    )
}

export default Home;
