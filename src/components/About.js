import React from 'react';
import ReactJS from './ReactJS.jpg'

const About = () => {
    return (
        <div className='aboutApp'>
            <h3> Miért pont React és a JavaScript? </h3>
            <img src={ReactJS} alt='reactJS' className='logo'/>
            <p>
                Azért választottam ezt a technológiát, mert napjainkban népszerű és divatos.
                A megalkotott felület letisztult, kényelmesen kezelhető mégis interaktív és esztétikus.
                Egy CRUD alkalmazás összes funkcióját tartalmazza.
            </p>
            <h3> A Reactről röviden</h3>
            <p>
                A Google és a Facebook által fejlesztett React népszerű technológia, amivel leghatékonyabban interaktív single-page alkalmazásokat lehet készíteni.
                Csupán <a href="https://reactjs.org/">React</a>-et használva nem lehet web alkalmazást készíteni, mivel ezt nézetek készítésére tervezték (ezt jelzi az MVC-ben a 'V'), komponens alapú nézeteket csinálhatunk,
                amelyek adatait a gyermek nézeteknek is át lehet adni. Az Angulartól eltérően, a React komponensek csak JavaScript függvények, tetszőleges számú inputtal és egy outputtal.
            </p>
            <p>
                Minden React komponens tetszőleges számú bemenetet fogad, ami egy props nevezetű objektumban tárolódik.
                Továbbá van egy render metódusa, és ahogy a neve is mutatja, ez a metódus határozza meg, hogy mi legyen megjelenítve amikor a komponens meghívódik.
                Minden komponens egy belső állapotot menedzsel (a this.state segítségével), és amikor az állapot változik, meghívódik az adott komponens render függvénye.
                React-el XML/HTML mezőket lehet a JavaScript fileba írni, és ezt a JSX-en keresztül tehetjük meg, ami szintaxis kiterjesztést nyúlt a JavaScript számára.
                Egy fordító eszközt kell használnunk, mint pl. a Babel, ami a JSX kódunkat JavaScript-re fordítja, amit már a böngésző is ért.
                Bár a JSX használata javasolt, maradhatunk a React.createElement() függvényhívásnál is, ha nem akarunk HTML tag-eket ágyazni a JavaScript-be.
                Továbbá akár az ES6 szabványt, akár a hagyományos JavaScript nyelvet is használhatjuk, amikor React-ben dolgozunk.
                A React kevésbé támogatja a típus ellenőrzést, mivel a mögötte lévő ES6 nem támogatja ezt.
                Egyébként a típus ellenőrzést a prop-types könyvtárral lehet implementálni, amit a React csapata fejlesztett ki.
            </p>
            <p>
                A create-react-app a hivatalosan is támogatott módja annak, hogy React app-ot lehessen készíteni bármilyen konfig fájlok nélkül.
                <h5 align='center'> npm install -g create-react-app </h5>
                Ez egy működőképes alkalmazást készít, minden Babel és webpack függőséget figyelembe véve.
                Az npm start paranccsal el is indíthatod az appot a böngészőben, a szkripteket megtalálod a package.json file-ban.
            </p>
            <p>
                Az adat binding egy olyan funkció, ami az alkalmazás állapota (modellje) és a nézet közötti adat szinkronizációt valósítja meg.
                Az egyutas adatkötéssel bármilyen állapotváltozás az alkalmazásban automatikusan frissíti a nézetet.
                Viszont a kétutas adatkötés összeköti a tulajdonságot és eseményeket egy egyedi entitásba, tehát bármilyen módosítás a modelben frissíti a nézetet és fordítva.
                React-ben a tulajdonságok a szülőtől a gyerek komponensek felé adódnak át, amit úgy is ismerhetünk, mint az egyirányú vagy fentről lefelé irányuló adatfolyam.
                Egy komponens állapota beágyazott és nem érhető el más komponensek számára, csak ha egy tulajdonságok keresztül egy gyerek komponensnek átadja,
                tehát ha a komponens állapota a gyerek komponens egy tulajdonságává válik.
                Ha Redux-al használjuk a React-et, a Redux dokumentációja jó útmutatást nyújt a szerver oldali renderelés felállításához.
                Úgy is be lehet állítani a React-et hogy a szerverről rendereljen, hogy a BrowserRouter és a StaticRouter komponenseket használjuk,
                amelyek a react-router könyvtárban érhetőek el.
            </p>
            <h3> Az app célja </h3>
            <p>
                A rohanó világban könnyen szétforgácsolódunk, elmaradnak feladataink, kevésbé figyelünk.
                Egy számítógép mellett töltött nap hasznos kiegészítője lehet egy Teendő számontartó alkalmazás,
                így remélhetőleg könnyebben managelhető az időnk.
                Az app 3 példával van feltöltve, de természetesen sajátokat is létrehozhatunk,
                ezeket lehet törölni, módosítani is, így teendőinket jól áttekinthetően tárolhatjuk elektronikus formában.
            </p>
            <h3> További fejlesztési lehetőségek </h3>
            <p>
                A későbbiek során hozzákapcsolható lenne egy adatbázis is, ami az ablak bezárása után is gondoskodna a mentésről,
                akár emlékeztető beépítését is érdemes lenne számba venni.
            </p>
        </div>
    );
}

export default About;
