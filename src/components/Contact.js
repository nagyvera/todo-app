import React from 'react';

const Contact = () => {
    return (
        <div>
            <table className = 'contactTable'>
                <tr>
                    <th>Fejlesztő</th>
                    <th>Telefonszám</th>
                    <th>E-mail cím</th>
                </tr>
                <tr>
                    <td>Nagy Veronika</td>
                    <td>+36 70/88-44-534</td>
                    <td>nagyvera@gamma.ttk.pte.hu</td>
                </tr>
            </table>
        </div>
    );
}

export default Contact;

